<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Route::get('/murid', function() {
    return view('murid.dashboard.index');
});

Route::get('/guru', function() {
    return view('guru.dashboard.index');
});

Auth::routes();


Route::get('/guru/cek-paket', 'PaketController@index')->name('guru.paket.index');
Route::get('/guru/buat-paket', 'PaketController@create')->name('guru.paket.create');
Route::post('/guru/cek-paket', 'PaketController@store')->name('guru.paket.store');
Route::get('/guru/cek-paket/{paket}', 'PaketController@show')->name('guru.paket.show');
Route::put('/guru/cek-paket/{paket}', 'PaketController@update')->name('guru.paket.update');
Route::delete('/guru/cek-paket/{paket}', 'PaketController@destroy')->name('guru.paket.destroy');

Route::get('/guru/cek-paket/{paket}/buat-soal', 'SoalController@create')->name('guru.soal.create');
Route::post('/guru/cek-paket/{paket}', 'SoalController@store')->name('guru.soal.store');
Route::get('/guru/cek-paket/{paket}/{soal}/edit', 'SoalController@edit')->name('guru.soal.edit');
Route::put('/guru/cek-paket/{paket}/{soal}', 'SoalController@update')->name('guru.soal.update');
Route::delete('/guru/cek-paket/{paket}/{soal}', 'SoalController@destroy')->name('guru.soal.destroy');

Route::get('/murid/ujian', 'UjianController@index')->name('murid.ujian.index');
Route::get('/murid/daftar-ujian', 'UjianController@create')->name('murid.ujian.create');
Route::post('/murid/ujian', 'UjianController@store')->name('murid.ujian.store');
Route::get('/murid/ujian/{paket}/{ujian}', 'UjianController@show')->name('murid.ujian.show');
Route::put('/murid/ujian/{paket}/{ujian}', 'UjianController@update')->name('murid.ujian.update');

Route::get('/murid/cek-ujian/rank/{ranking}', 'RankingController@show')->name('murid.ranking.show');
Route::get('/murid/cek-ujian/rekap/{penilaian}', 'PenilaianController@show')->name('murid.penilaian.show');

Route::resource('kunci', 'KunciController');