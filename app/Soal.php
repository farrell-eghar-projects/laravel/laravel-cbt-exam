<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Soal extends Model
{
    protected $table = 'soal';

    protected $guarded = [];

    // Eloquent Relationship one-to-many
    public function paket() {
        return $this->belongsTo('App\Paket', 'paket_id');
    }

    // Eloquent Relationship one-to-many
    public function kunci() {
        return $this->hasMany('App\Kunci', 'soal_id');
    }

    // Eloquent Relationship many-to-many
    public function penilaian() {
        return $this->belongsToMany('App\Ujian', 'penilaian', 'ujian_id', 'soal_id');
    }
}
