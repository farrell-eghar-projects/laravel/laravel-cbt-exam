<?php

namespace App\Http\Controllers;

use App\Paket;
use App\Soal;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class PaketController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pakets = Paket::where('user_id', Auth::user()->id)->get();
        
        $count = [];
        foreach ($pakets as $key => $value) {
            $soal = Soal::where('paket_id', $value->id)->get();
            $count[] = count($soal);
        }

        return view('guru.paket.index', [
            'pakets' => $pakets,
            'soal' => $count
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('guru.paket.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'paket' => 'required|unique:paket',
            'password' => 'required',
            'durasi' => 'required|numeric'
        ]);

        $id = Auth::id();
        Paket::create([
            'paket' => $request['paket'],
            'password' => $request['password'],
            'durasi' => $request['durasi'],
            'user_id' => $id
        ]);

        return redirect()->route('guru.paket.index')->with('success', 'Paket successfully added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Paket $paket)
    {
        $soal = Soal::where('paket_id', $paket->id)->get();

        return view('guru.paket.show', compact('paket', 'soal'));
    }

    // /**
    //  * Show the form for editing the specified resource.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function edit(Paket $paket)
    // {
    //     return view('paket.edit', compact('paket'));
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Paket $paket)
    {
        if($request->paket == $paket->paket) {
            $request->validate([
                'paket' => 'bail|required',
                'password' => 'bail|required',
                'durasi' => 'required|numeric'
            ]);
        } else {
            $request->validate([
                'paket' => 'bail|required|unique:paket',
                'password' => 'bail|required',
                'durasi' => 'required|numeric'
            ]);
        }

        $paket->update($request->all());

        return redirect()->route('guru.paket.show', ['paket' => $paket->id])->with('success', 'Paket updated successfully ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Paket $paket)
    {
        $paket->delete();

        return redirect()->route('guru.paket.index')->with('success','Paket deleted successfully');
    }
}
