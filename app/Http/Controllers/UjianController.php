<?php

namespace App\Http\Controllers;

use App\Ujian;
use App\Paket;
use App\User;
use App\Penilaian;
use App\Soal;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class UjianController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ujian = Ujian::where('user_id', Auth::user()->id)->get();
        $countSoal = [];
        $countMurid = [];
        $paket = [];
        foreach ($ujian as $key => $value) {
            $soal = Soal::where('paket_id', $value->paket_id)->get(); 
            $countSoal[] = count($soal);
            $murid = Ujian::where('paket_id', $value->paket_id)->get();
            $countMurid[] = count($murid);
            $cek = Paket::where('id', $value->paket_id)->first();
            $paket[] = $cek;
        }

        return view('murid.ujian.index', [
            'ujian' => $ujian,
            'soal' => $countSoal,
            'murid' => $countMurid,
            'paket' => $paket
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('murid.ujian.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'paket' => 'required',
            'password' => 'required'
        ]);

        $id = $request->paket;
        $pass = $request->password;
        
        $paket = Paket::where([
            ['paket', '=', $id],
            ['password', '=', $pass]
        ])->first();
        
        $check = Ujian::where([
            ['paket_id', '=', $paket->id],
            ['user_id', '=', Auth::user()->id]
        ])->first();

        $ujian = 0;
        if ($paket->exists()) {
            if ($check === null) {

                $ujian = Ujian::create([
                    'paket_id' => $paket->id,
                    'user_id' => Auth::user()->id,
                    'nilai' => 0,
                    'submit' => 0
                ]);

            }

            $ujian = Ujian::where([
                ['paket_id', '=', $paket->id],
                ['user_id', '=', Auth::user()->id]
            ])->first();

            if ($ujian->submit == 1) {
                return redirect()->route('murid.ujian.create')->with('fail', 'You Have Joined This Quiz');
            }
            
            return redirect()->route('murid.ujian.show', ['paket' => $paket, 'ujian' => $ujian])->with('success', 'Ujian joined successfully');
        } else {
            return redirect()->route('murid.ujian.create')->with('fail', 'No Package Found');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Paket $paket, Ujian $ujian)
    {
        return view('murid.ujian.show', compact('paket', 'ujian'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Ujian $ujian)
    {
        return view('ujian.edit', compact('ujian'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Paket $paket, Ujian $ujian)
    {
        $ujian->submit = 1;
        
        $soal = [];
        foreach ($paket->soal as $key => $value) {
            $soal[] = $value->id;
        }

        $ujian->penilaian()->sync($soal);

        $nilai = [];
        foreach ($soal as $key => $value) {
            $penilaian = Penilaian::where([
                ['ujian_id', '=', $ujian->id],
                ['soal_id', '=', $value]
            ])->first();
            $penilaian->jawaban = $request->jawaban[$key];
            $cocok = Soal::find($value);
            if ($penilaian->jawaban == $cocok->jawaban) {
                $penilaian->nilai = $cocok->nilai;
            } else {
                $penilaian->nilai = 0;
            }
            $nilai[] = $penilaian->nilai;
            $penilaian->save();
        }

        foreach ($nilai as $key => $value) {
            $ujian->nilai += $value;
        }
        $ujian->save();

        return redirect()->route('murid.ujian.index')->with('success', 'Ujian updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ujian $ujian)
    {
        $ujian->delete();

        return redirect()->route('ujian.index')->with('success', 'Ujian deleted successfully');
    }
}
