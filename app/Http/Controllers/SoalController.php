<?php

namespace App\Http\Controllers;

use App\Soal;
use App\Paket;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class SoalController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $soals = Soal::latest()->paginate(10);

        return view('soal.index', [
            'soals' => $soals
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Paket $paket)
    {
        return view('guru.soal.create', compact('paket'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Paket $paket, Request $request)
    {
        $request->validate([
            'soal' => 'required',
            'jawaban' => 'required',
            'nilai' => 'required|numeric',
        ]);

        $id = $paket->id;
        Soal::create([
            'soal' => $request['soal'],
            'jawaban' => $request['jawaban'],
            'nilai' => $request['nilai'],
            'paket_id' => $id
        ]);

        $soal = Soal::where('paket_id', $paket->id)->get();

        return view('guru.paket.show', compact('paket', 'soal'))->with('success','Soal created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Soal $soal)
    {
        return view('soal.show',compact('soal'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Paket $paket, Soal $soal)
    {
        return view('guru.soal.edit', compact('paket', 'soal'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Paket $paket, Soal $soal)
    {
        $request->validate([
            'soal' => 'required',
            'jawaban' => 'required',
            'nilai' => 'required|numeric',
        ]);

        $soal->update($request->all());
        $soal = Soal::where('paket_id', $paket->id)->get();

        return view('guru.paket.show', compact('paket', 'soal'))->with('success','Soal created successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Paket $paket, Soal $soal)
    {
        $soal->delete();

        return redirect()->route('guru.paket.show', compact('paket', 'soal'))->with('success','Soal deleted successfully');
    }
}
