<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // Eloquent Relationship one-to-one
    public function peran() {
        return $this->hasOne('App\Peran', 'user_id');
    }

    // Eloquent Relationship one-to-many
    public function paket() {
        return $this->hasMany('App\Paket', 'user_id');
    }

    // Eloquent Relationship many-to-many
    public function ujian() {
        return $this->belongsToMany('App\Paket', 'ujian', 'user_id', 'paket_id');
    }
}
