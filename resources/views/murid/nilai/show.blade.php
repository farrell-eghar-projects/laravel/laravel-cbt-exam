@extends('layouts.murid')

@section('content')
<!-- Default Basic Forms Start -->
<div class="pd-20 card-box mt-3">
    <div class="clearfix">
        <div class="pull-left">
            <h3 class="text-blue h3">Quiz Recap</h3>
        </div>
    </div>
    @forelse ($soal as $key => $item)
        <div class="mt-5">
            <div class="clearfix">
                <div class="pull-left">
                    <h4 class="h4">Question {{ $key + 1 }} (Your Score: {{ $nilai[$key] }})</h4>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12 col-md-12">
                    <label class="form-control col-form-label">{{ $item->soal }}</label>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12 col-md-12">
                    <input class="form-control" name="jawaban[]" value="{{ $jawaban[$key] }}" disabled placeholder="Enter Answer">
                </div>
            </div>
            <div class="clearfix">
                <div class="pull-left">
                    <h4 class="h4">Correct Answer (Score: {{ $item->nilai }})</h4>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12 col-md-12">
                    <input class="form-control" name="jawaban[]" value="{{ $item->jawaban }}" disabled placeholder="Enter Answer">
                </div>
            </div>
        </div>    
    @empty
        <h4 align="center">No Questions</h4>
    @endforelse
</div>
@endsection