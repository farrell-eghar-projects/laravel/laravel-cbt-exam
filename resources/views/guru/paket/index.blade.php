@extends('layouts.guru')

@section('content')
    <!-- Responsive tables Start -->
    <div class="pd-20 card-box mb-30">
        <div class="clearfix mb-20">
            <div class="pull-left">
                <h4 class="text-blue h4">Display All Packages</h4>
                <p>This page displays the information of all packages you have, including Package Name, Password, Quiz Duration, Students Joined the Quiz. In here, you can also create new package and view more details into your package information</p>
            </div>
            <div class="pull-right">
                <a href="{{ route('guru.paket.create') }}" class="btn btn-primary btn-sm scroll-click">Create New</a>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Package Name</th>
                        <th scope="col">Questions</th>
                        <th scope="col">Delete Package</th>
                        <th scope="col">View Details</th>
                        <th scope="col">View Results</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($pakets as $key => $item)
                        <tr>
                            <th scope="row">{{ $key + 1 }}</th>
                            <td>{{ $item->paket }}</td>
                            <td>{{ $soal[$key] }}</td>
                            <form action="/guru/cek-paket/{{ $item->id }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <td><input type="submit" value="Delete" class="btn btn-danger"></td>
                            </form>
                            <td><a href="/guru/cek-paket/{{ $item->id }}" class="btn btn-primary">View</a></td>
                            <td><a href="/guru/cek-ujian/{{ $item->id }}" class="btn btn-success">view</a></td>
                        </tr>    
                    @empty
                        <tr>
                            <td colspan="6" align="center">No Package Available</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
    <!-- Responsive tables End -->
@endsection