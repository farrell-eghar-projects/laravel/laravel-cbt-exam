@extends('layouts.guru')

@section('content')
<!-- Responsive tables Start -->
    <div class="pd-20 card-box mb-30">
        <div class="clearfix mb-20">
            <div class="pull-left">
                <h4 class="text-blue h4">Package Details</h4>
                <form action="{{ route('guru.paket.update', ['paket' => $paket->id]) }}" method="POST">
                    @method('PUT')
                    @csrf
                    <div class="form-group row mt-4">
                        <label class="col-sm-12 col-md-2 col-form-label">Package Name</label>
                        <div class="col-sm-10 col-md-8">
                            <input class="form-control" name="paket" value="{{ old('paket', $paket->paket) }}" type="paket" placeholder="Unique">
                        </div>
                        <div class="col-sm-2 col-md-2 pull-right">
                            <button type="submit" class="btn btn-primary btn-sm">Edit</button>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-2 col-form-label">Package Password</label>
                        <div class="col-sm-10 col-md-8">
                            <input class="form-control" name="password" value="{{ old('password', $paket->password) }}" placeholder="Enter Password">
                        </div>
                        <div class="col-sm-2 col-md-2 pull-right">
                            <button type="submit" class="btn btn-primary btn-sm">Edit</button>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-2 col-form-label">Quiz Duration (Minutes)</label>
                        <div class="col-sm-10 col-md-8">
                            <input class="form-control" name="durasi" placeholder="120" value="{{ old('durasi', $paket->durasi) }}" type="durasi">
                        </div>
                        <div class="col-sm-2 col-md-2 pull-right">
                            <button type="submit" class="btn btn-primary btn-sm">Edit</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="clearfix mb-0">
                <div class="pull-left mb-4">
                    <h4 class="text-blue h4">Display All Questions</h4>
                    <p>This page displays the information of all questions inside the package, including Questions, Answers, Quiz Duration, Score For Each Questions. In here, you can also create new question and edit your question</p>
                </div>
                <div class="pull-right">
                    <a href="{{ route('guru.soal.create', ['paket' => $paket->id]) }}" class="btn btn-primary btn-sm scroll-click">Create New</a>
                </div>
                <div class="pull-left mt-3">
                    <h6 class="text-blue">Total Questions: {{ count($soal) }}</h6>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">Questions</th>
                        <th scope="col">Answers</th>
                        <th scope="col">Score</th>
                        <th scope="col">Delete</th>
                        <th scope="col">Edit</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($soal as $key => $item)
                        <tr>
                            <td>{{ $item->soal }}</td>
                            <td>{{ $item->jawaban }}</td>
                            <td>{{ $item->nilai }}</td>
                            <form action="{{ route('guru.soal.destroy', ['paket' => $paket->id, 'soal' => $item->id]) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <td><input type="submit" value="Delete" class="btn btn-danger btn-sm"></td>
                            </form>
                            <td>
                                <a href="{{ route('guru.soal.edit', ['paket' => $paket->id, 'soal' => $item->id]) }}" class="btn btn-danger btn-sm">Edit</a>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="5" align="center">No Questions</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
<!-- Responsive tables End -->
@endsection