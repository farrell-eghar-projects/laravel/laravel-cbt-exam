@extends('layouts.guru')

@section('content')
    <!-- Default Basic Forms Start -->
    <div class="pd-20 card-box mt-3">
        <form action="{{ route('guru.soal.update', ['paket' => $paket->id, 'soal'=> $soal->id]) }}" method="POST">
            @method('PUT')
            @csrf
            <div class="clearfix">
                <div class="pull-left">
                    <h4 class="text-blue h4">Update Old Question</h4>
                </div>
                <div class="pull-right">
                    <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                </div>
            </div>
            <div class="form-group row mt-4">
                <label class="col-sm-12 col-md-2 col-form-label">Question</label>
                <div class="col-sm-12 col-md-10">
                    <input class="form-control" name="soal" type="soal" value="{{ old('soal', $soal->soal) }}" placeholder="Input Question Here!">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-12 col-md-2 col-form-label">Answer</label>
                <div class="col-sm-12 col-md-10">
                    <input class="form-control" name="jawaban" value="{{ old('jawaban', $soal->jawaban) }}" placeholder="Input Answer Here!">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-12 col-md-2 col-form-label">Score</label>
                <div class="col-sm-12 col-md-10">
                    <input class="form-control" name="nilai" value="{{ old('nilai', $soal->nilai) }}" placeholder="100" type="nilai">
                </div>
            </div>
        </form>
    </div>
@endsection